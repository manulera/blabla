# Credits

A simulation of the cytoskeleton was started in 1995 at Princeton and pursued at [ESPCI](http://www.espci.fr) until 1998. Development continued within the Nedelec group at [EMBL](http://www.embl.org) from 1999 to 2017. Current  development is now conducted by the Nedelec group at [Cambridge University](http://www.cam.ac.uk) and in other groups.  

## History

- 1995 First version
- 1996 Filament bending elasticity
- 1999 Project named `Cytosim`
- 2001 Implicit integration scheme
- 2012 Scripting language
- 2018-2019 version 4
  

## Contributors
 
- Francois Nedelec        1995-  
- Dietrich Foethke        2003-2007  
- Cleopatra Kozlowski     2003-2007  
- Elizabeth Loughlin      2006-2010  
- Ludovic Brun            2008-2010  
- Beat Rupp               2008-2011  
- Jonathan Ward           2008-2014  
- Antonio Politi          2010-2012  
- Andre-Claude Clapson    2011-2013  
- Serge Dmitrieff         2013-  
- Julio Belmonte          2014-  

### Contact:
 
 Email: feedback@cytosim.org  
 
 
 


