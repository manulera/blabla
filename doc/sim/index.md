# Cytosim's model

***Unfinished***

Table of Content:

* [The configuration file](config.md)
* [Commands](commands.md)
* [Objects](objects.md)
* [Placement](placement.md)
* [Confinement and Space](spaces.md)
* [Fibers](fibers.md)
* [Fiber assembly dynamics](fiber_dynamics.md)
* [Stochastic events](stochastic.md)
* [Stochastic forces](forces.md)
* [Steric interactions](steric.md)
* [Graphical display](graphics.md)
* [Parameters](parameters.md)
* [Physical Units](units.md)

