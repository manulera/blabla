# Articles describing Cytosim and related work

- [Collective Langevin Dynamics of Flexible Cytoskeletal Fibers](http://dx.doi.org/10.1088/1367-2630/9/11/427)  

- [Preconfig: A Versatile Configuration File Generator for Varying Parameters](http://doi.org/10.5334/jors.156)  


# Articles from other groups using Cytosim

*Please, let us know if we are missing citations here*

2019

- [Collective effects of yeast cytoplasmic dynein based microtubule transport](http://dx.doi.org/10.1039/c8sm01434e)
- [Chromosomes function as a barrier to mitotic spindle bipolarity in polyploid cells](https://www.biorxiv.org/content/10.1101/572099v1.full)  

2018

- [Mechanical positioning of multiple nuclei in muscle cells](https://doi.org/10.1371/journal.pcbi.1006208)
- [Role of External Forcing in Directional Instability of Microtubule Transport](http://dr.iiserpune.ac.in:8080/xmlui/handle/123456789/987)
- [Entropy production rate is maximized in non- contractile actomyosin](http://dx.doi.org/10.1038/s41467-018-07413-5)

2017

- [Microtubule stabilization drives 3D centrosome migration to initiate primary ciliogenesis](https://doi.org/10.1083/jcb.201610039)
- [A Versatile Framework for Simulating the Dynamic Mechanical Structure of Cytoskeletal Networks](https://doi.org/10.1016/j.bpj.2017.06.003)
- [Centrosome Centering and Decentering by Microtubule Network Rearrangement](https://doi.org/10.4172/2168-9431.1000158)
- [Computer simulations reveal mechanisms that organize nuclear dynein forces to separate centrosomes](http://molbiolcell.org/cgi/doi/10.1091/mbc.E16-12-0823)

2016

- [A Motor-Gradient and Clustering Model of the Centripetal Motility of MTOCs in Meiosis I of Mouse Oocytes](https://doi.org/10.1371/journal.pcbi.1005102)

2015

- [Dynamic reorganization of the actin cytoskeleton](http://dx.doi.org/10.12688/f1000research.6374.1)

2013

- [Analysis of the local organization and dynamics of cellular actin networks](http://dx.doi.org/10.1083/jcb.201210123)

2012

- [Spatial and Temporal Sensing Limits of Microtubule Polarization in Neuronal Growth Cones by Intracellular Gradients and Forces](http://dx.doi.org/10.1016/j.bpj.2012.10.021)
